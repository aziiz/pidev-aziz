package managedeBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.pi.entity.Evaluation;
import tn.esprit.service.impl.EvaluationService;

@ManagedBean(name="chartsData")
@ApplicationScoped
public class ChartDataBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	EvaluationService evaluationService;

	private List<Evaluation> evaluations;

	static List<KeyValue> pieDataList;


	public List<KeyValue> getPieDataList() {
		pieDataList = new ArrayList<ChartDataBean.KeyValue>();
		evaluations =  evaluationService.getAllEvaluations();

//		System.out.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee" + evaluationService.getAllEvaluations().size());
//		for(int i=0 ; i<evaluationService.getAllEvaluations().size() ; i++) {
//			System.out.println("nooooooote " +evaluations.get(0).getNote()+ "  "+ evaluations.get(0).getType().toString() );
//			pieDataList.add(new KeyValue("hahahah", Integer.toString(evaluations.get(0).getNote())));
//
// 		}
		for(Evaluation e : evaluationService.getAllEvaluations()) {
			pieDataList.add(new KeyValue(e.getType().toString(), Integer.toString(e.getNote())));

 		}	

		return pieDataList;
	}


	public List<Evaluation> getEvaluations() {
		return evaluationService.getAllEvaluations();
	}


	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}


	public static class KeyValue {
		String key;
		String value;
		public KeyValue(String key, String value) {
			super();
			this.key = key;
			this.value = value;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
	}
}