package managedeBean;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import tn.esprit.pi.entity.EvaluationCriteria;
import tn.esprit.service.impl.EvaluationCriteriaService;

 

@ManagedBean(name="criteriaBean")
@SessionScoped
public class CriteriaBean implements Serializable {

	
	@EJB
	EvaluationCriteriaService criteriaService;


	private String criteriaName;
	private List<EvaluationCriteria> criterias;
	
	
	
	public String deleteCriteria(int criteriaId) {
		criteriaService.deleteCriteria(criteriaId);
		return "/index?faces-redirect=true";
	}

	public String addCriteria() {
		criteriaService.addCriteria(new EvaluationCriteria(criteriaName, 0));
		return "/index?faces-redirect=true";
	}

	public List<EvaluationCriteria> getCriterias() {
		List<EvaluationCriteria> allCriterias = criteriaService.getAllCriterias();
		return allCriterias;
	}

	public String getCriteriaName() {
		return criteriaName;
	}

	public void setCriteriaName(String criteriaName) {
		this.criteriaName = criteriaName;
	}

	public void setCriterias(List<EvaluationCriteria> criterias) {
		this.criterias = criterias;
	}
	public EvaluationCriteriaService getCriteriaService() {
		return criteriaService;
	}

	public void setCriteriaService(EvaluationCriteriaService criteriaService) {
		this.criteriaService = criteriaService;
	}
	
	
	
	
}
