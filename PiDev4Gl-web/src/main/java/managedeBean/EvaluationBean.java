package managedeBean;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.pi.entity.Employe;
import tn.esprit.pi.entity.Evaluation;
import tn.esprit.pi.entity.EvaluationCriteria;
import tn.esprit.pi.entity.EvaluationType;
 import tn.esprit.service.impl.EvaluationService;

 
@ManagedBean(name = "evaluationBean")
@SessionScoped
public class EvaluationBean {
	
	private String description; 
	private EvaluationType type;
	private EvaluationCriteria criteria;
	private List<Evaluation> evaluations;
	private String dependability ;
	private String ponctuality ;
	private Integer evaluationIdToBeUpdated;
	private Employe manager;
	private Employe employe;
	@ManagedProperty(value = "#{criteriaBean}")
	private CriteriaBean criteres;
	private List<EvaluationCriteria> criterias;
	public Employe getManager() {
		return manager;
	}

	public CriteriaBean getCriteres() {
		return criteres;
	}

	public void setCriteres(CriteriaBean criteres) {
		this.criteres = criteres;
	}

	public List<EvaluationCriteria> getCriterias() {
		return criterias;
	}

	public void setCriterias(List<EvaluationCriteria> criterias) {
		this.criterias = criterias;
	}

	public void setManager(Employe manager) {
		this.manager = manager;
	}

	public Integer getEvaluationIdToBeUpdated() {
		return evaluationIdToBeUpdated;
	}
	
	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	private  int note;
	
	public String getDependability() {
		return dependability;
	}

	public void setDependability(String dependability) {
		this.dependability = dependability;
	}

	public String getPonctuality() {
		return ponctuality;
	}

	public void setPonctuality(String ponctuality) {
		this.ponctuality = ponctuality;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public String getDescription() {
		return description;
	}
	
	public EvaluationCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(EvaluationCriteria criteria) {
		this.criteria = criteria;
	}
	 
	private Date date=new Date();
 
	public Date getDate() {
		return date;
	}

	public EvaluationType getType() {
		return type;
	}

	public void setType(EvaluationType type) {
		this.type = type;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	 
	 
	 
 

	public void setDescription(String description) {
		this.description = description;
	}
 
	@EJB
	EvaluationService evaluationService; 

	public String addEvaluation() {
			 	
		evaluationService.ajouterEvaluation(new Evaluation(description,type,date,criteres.getCriterias(),dependability,ponctuality,note,manager,employe));
		return "/showevaluation?faces-redirect=true";
 
	}  
	public String redirectEvaluation() {
		 
		return "/showevaluation?faces-redirect=true";
 
	}  
	public List<Evaluation> getEvaluations(){
		evaluations = evaluationService.getAllEvaluations(); 
		return evaluations;
		} 
	public void removeEvaluation(int evaluationId)
	{
		evaluationService.deleteEvaluationById(evaluationId); 
	}
	public void setEvaluationIdToBeUpdated(Integer evaluationIdToBeUpdated) {
		this.evaluationIdToBeUpdated = evaluationIdToBeUpdated;
	}
	 public void modifier(Evaluation e)
	{this.setDescription(e.getDescription());
	 
	this.setDependability(e.getDependability());
	this.setNote(e.getNote());
	this.setDate(e.getDate());
		this.setEvaluationIdToBeUpdated(e.getId());
	} 
	public void mettreAjourEvaluation(){
	 	evaluationService.updateEvaluation(new Evaluation(evaluationIdToBeUpdated, description,type,date,dependability,ponctuality,note));
	}

}
