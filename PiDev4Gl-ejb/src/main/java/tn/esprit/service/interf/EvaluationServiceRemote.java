package tn.esprit.service.interf;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pi.entity.Evaluation;
@Remote
public interface EvaluationServiceRemote {
	
	public void ajouterEvaluation(Evaluation e);
	public List<Evaluation> getAllEvaluations();

}
