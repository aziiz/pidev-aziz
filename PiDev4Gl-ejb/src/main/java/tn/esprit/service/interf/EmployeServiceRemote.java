package tn.esprit.service.interf;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pi.entity.Employe;

@Remote
public interface EmployeServiceRemote {
	public List<Employe> getAllUsers();
	public List<String> getAllUserNames();
}
