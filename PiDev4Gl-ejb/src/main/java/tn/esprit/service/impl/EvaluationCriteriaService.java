package tn.esprit.service.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.pi.entity.Evaluation;
import tn.esprit.pi.entity.EvaluationCriteria;
import tn.esprit.pi.entity.EvaluationType;

 


@Stateless
@LocalBean
public class EvaluationCriteriaService   {
	@PersistenceContext(unitName = "imputation-ejb")

	 
	EntityManager em;

	public List<EvaluationCriteria> getAllCriterias() {
		List<EvaluationCriteria> criterias = em.createQuery("SELECT e FROM EvaluationCriteria e", EvaluationCriteria.class).getResultList();
		return criterias;
	}

	 
	public void addCriteria(EvaluationCriteria ec) {
		Boolean found = false;
		String crName = ec.getCriteria();
		try{
			TypedQuery<String> query = em.createQuery("SELECT e.criteria from EvaluationCriteria e", String.class);

			for(String name: query.getResultList()) {
				if(name.equals(crName)) {
					found=true;	
				}
			}
			if(found == false) {
				em.persist(ec);
			}
			else {
				System.out.println("La critére existe déja !");
			}

		}catch(Exception e) {
			System.out.println(e);
		}
	
		
		
	}

	 
	public void deleteCriteria(int criteriaId) {
		EvaluationCriteria e = em.find(EvaluationCriteria.class,criteriaId);

		em.remove(e);
	 
		
	}

	 
	public List<EvaluationCriteria> getCriteriasByEvaluationType(EvaluationType evaluationType) {
		TypedQuery<EvaluationCriteria> query = em.createQuery(" SELECT c EvaluationCriteria c join Evaluation e where e.type =:type", EvaluationCriteria.class);
		query.setParameter("type",evaluationType);
		return query.getResultList();
	}

	 
	public void affectCriteriaToEvaluation(EvaluationCriteria cr, Evaluation e) {
		// TODO Auto-generated method stub
		
	}
	
	

}
