package tn.esprit.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.pi.entity.Employe;
 
@Stateless
@LocalBean
public class EmployeService {
	@PersistenceContext(unitName = "imputation-ejb")
	 

 
	EntityManager em;
	public Employe getEmployeByEmailAndPassword(String login, String password) {
		
		TypedQuery<Employe> query = em.createQuery("select e from Employe e where e.login=:login AND e.password=:password", Employe.class); 

		query.setParameter("login", login); 
		query.setParameter("password", password); 

		Employe employe = null; 
		try { employe = query.getSingleResult(); }
		catch (Exception e) { System.out.println("Erreur : " + e); }

		return employe;
		}
	public  List<Employe> getAllUsers() {
		List<Employe> users = em.createQuery("SELECT u  from Employe u",Employe.class).getResultList();
		return users;
	}
	public  List<String> getAllUserNames() {
		List<String> names = new ArrayList<>();
		for(Employe u : getAllUsers()) {
			names.add(u.getLogin());
		}
		return names;
	}
}
