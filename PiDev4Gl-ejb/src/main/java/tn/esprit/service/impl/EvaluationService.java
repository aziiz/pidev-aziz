package tn.esprit.service.impl;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

 import tn.esprit.pi.entity.Evaluation;
@Stateless
@LocalBean
public class EvaluationService {
	@PersistenceContext(unitName = "imputation-ejb")
	EntityManager em;
	public void ajouterEvaluation(Evaluation e){
		em.persist(e);
		
	}
	
	public List<Evaluation> getAllEvaluations() {
		List<Evaluation> emp = em.createQuery("Select e from Evaluation e", Evaluation.class).getResultList();
		return emp;
		}
	public void deleteEvaluationById(int evaluationId)
	{Evaluation e = em.find(Evaluation.class,evaluationId);

		em.remove(e);
	}
	public void updateEvaluation(Evaluation e)
	{
		 
	 
		em.merge(e);

	}
	
}
