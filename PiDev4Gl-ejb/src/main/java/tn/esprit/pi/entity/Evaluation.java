package tn.esprit.pi.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
 import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

 
 
 



@Entity
public class Evaluation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	 
	private String dependability ;
	private String ponctuality ;
	
	private  int note;
 
	@ManyToMany
	private List<EvaluationCriteria> criterias;

	public int getNote() {
		return note;
	}


	public void setNote(int note) {
		this.note = note;
	}


	public String getDependability() {
		return dependability;
	}


	public void setDependability(String dependability) {
		this.dependability = dependability;
	}


	public String getPonctuality() {
		return ponctuality;
	}


	public void setPonctuality(String ponctuality) {
		this.ponctuality = ponctuality;
	}


	@Enumerated(EnumType.STRING)	
	private EvaluationType type;
	
	 

	private String description; 
	
	private Date date ;
	
	
 


	public List<EvaluationCriteria> getCriterias() {
		return criterias;
	}


	public void setCriterias(List<EvaluationCriteria> criterias) {
		this.criterias = criterias;
	}


	public Evaluation(String description,EvaluationType type,Date date, String dependability,String ponctuality,int note) {
		super();
		this.description = description;
		this.type = type;
		this.date = date;
		this.criterias = criterias;
		this.dependability = dependability;
		this.ponctuality = ponctuality;
		this.note = note;
	}
	public Evaluation(int id,String description,EvaluationType type,Date date, String dependability,String ponctuality,int note) {
		super();
		this.id = id;
		this.description = description;
		this.type = type;
		this.date = date;
		this.criterias = criterias;
		this.dependability = dependability;
		this.ponctuality = ponctuality;
		this.note = note;
	}
	public Evaluation(String description,EvaluationType type,Date date,List<EvaluationCriteria> criterias,
			String dependability,String ponctuality,int note,Employe sender,Employe receiver) {
		super();
		 
		this.description = description;
		this.type = type;
		this.date = date;
		this.criterias = criterias;
		this.dependability = dependability;
		this.ponctuality = ponctuality;
		this.note = note;
		this.sender= sender;
		this.receiver = receiver;
	}
	
 
	
	@OneToOne(cascade = {CascadeType.ALL}, 
			fetch=FetchType.EAGER)
	private Employe sender;
	
	
	@OneToOne
	private Employe receiver;
	
 

	

	public Evaluation() {
		super();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


 


	public EvaluationType getType() {
		return type;
	}


	public void setType(EvaluationType type) {
		this.type = type;
	}


	public String evaluations() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}



	 

	public String getDescription() {
		return description;
	}


	 


	public Employe getSender() {
		return sender;
	}


	public void setSender(Employe sender) {
		this.sender = sender;
	}


	public Employe getReceiver() {
		return receiver;
	}


	public void setReceiver(Employe receiver) {
		this.receiver = receiver;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	
	
}
