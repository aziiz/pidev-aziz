package tn.esprit.pi.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="CONDIDATURE")
public class Condidature implements Serializable{

	
	private static final long serialVersionUID = -357738161698377833L;


	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="C_ID")
	int id;
	
	@Column(name="LETTREDEMOTIVATION")
	String letterDeMotivation;
	
	@Enumerated(EnumType.STRING)
	private Departement departement;
	
	@ManyToOne
	@JoinColumn(name="FK_EMP_ID"  )
	Employe employe; 

	@ManyToOne
	@JoinColumn(name="FK_M_ID")
	Mission mission; 

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLetterDeMotivation() {
		return letterDeMotivation;
	}

	public void setLetterDeMotivation(String letterDeMotivation) {
		this.letterDeMotivation = letterDeMotivation;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Condidature() {
		super();
	}
	
	
	
}
