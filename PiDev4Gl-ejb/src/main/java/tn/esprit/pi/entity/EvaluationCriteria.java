package tn.esprit.pi.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class EvaluationCriteria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5443351151396868724L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String criteria;
	
	private float note ;
	
	@ManyToMany(cascade = {CascadeType.REMOVE}, mappedBy="criterias", fetch = FetchType.EAGER)
	private List<Evaluation> evaluations;

	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public float getNote() {
		return note;
	}

	public void setNote(float note) {
		this.note = note;
	}

	public List<Evaluation> getEvaluations() {
		return evaluations;
	}

	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}

	public EvaluationCriteria(int id, String criteria, float note, List<Evaluation> evaluations) {
		super();
		this.id = id;
		this.criteria = criteria;
		this.note = note;
		this.evaluations = evaluations;
	}

	public EvaluationCriteria(String criteria, float note) {
		this.criteria = criteria;
		this.note = note;
	}

	public EvaluationCriteria() {
	}
	
	
	
	
	
	
	
	
	
}
