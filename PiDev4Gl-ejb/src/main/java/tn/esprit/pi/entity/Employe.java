package tn.esprit.pi.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

 @Entity
 
public class Employe implements Serializable{
	
	
private static final long serialVersionUID = -357738161698377833L;

	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="Empl_ID")
	int id;
	String login;
	 
	String password;

	@OneToMany(mappedBy="receiver")

	private List<Evaluation> evaluations;
 

	 

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Evaluation> getEvaluations() {
		return evaluations;
	}

	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}

	@OneToMany(mappedBy="employe" ,  cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<Commentaire> commentaire;
	
	@OneToMany(mappedBy="employe" ,  cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<Condidature> condidature;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Set<Commentaire> getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(Set<Commentaire> commentaire) {
		this.commentaire = commentaire;
	}

	public Set<Condidature> getCondidature() {
		return condidature;
	}

	public void setCondidature(Set<Condidature> condidature) {
		this.condidature = condidature;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employe() {
		super();
	}
	
	
	
	
}
