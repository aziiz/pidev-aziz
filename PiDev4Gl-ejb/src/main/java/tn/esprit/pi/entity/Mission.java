package tn.esprit.pi.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="MISSION")
public class Mission implements Serializable{

	private static final long serialVersionUID = -357738161698377833L;


	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="M_ID")
	int id;
	
	@Column(name="M_NAME")
	String name;
	
	
	@Column(name="M_DATEDEBUT")
	Date dateDebut;
	
	
	@Column(name="M_DATEFIN")
	Date dateFin;
	
	
	@Column(name="M_PAYS")
	String pay;
	
	@Column(name="M_DEVICE")
	float device;
	
	@Column(name="M_RATE")
	int rate;
	
	@Column(name="M_FRAISHEBERGEMENT")
	float fraisHebergement;
	
	@Column(name="M_FRAISTRANSPORT")
	float fraisTransport;
	
	@Column(name="M_FRAISJOURNEE")
	float fraisJournee;
	
	@Column(name="M_FRAISRESTAURATION")
	float fraisRestauration;
	
	@Enumerated(EnumType.STRING)
	private Departement departement;
	

	@OneToMany(mappedBy="mission",  cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<Commentaire> commentaire;
	
	@OneToMany(mappedBy="mission",  cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<Condidature> condidature;
	
	@OneToMany(mappedBy="mission" , cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<Facture> facture;
	
	
	public Set<Commentaire> getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(Set<Commentaire> commentaire) {
		this.commentaire = commentaire;
	}
	public Set<Condidature> getCondidature() {
		return condidature;
	}
	public void setCondidature(Set<Condidature> condidature) {
		this.condidature = condidature;
	}
	public Set<Facture> getFacture() {
		return facture;
	}
	public void setFacture(Set<Facture> facture) {
		this.facture = facture;
	}
	public Departement getDepartement() {
		return departement;
	}
	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public String getPay() {
		return pay;
	}
	public void setPay(String pay) {
		this.pay = pay;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public float getDevice() {
		return device;
	}
	public void setDevice(float device) {
		this.device = device;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	public float getFraisHebergement() {
		return fraisHebergement;
	}
	public void setFraisHebergement(float fraisHebergement) {
		this.fraisHebergement = fraisHebergement;
	}
	public float getFraisTransport() {
		return fraisTransport;
	}
	public void setFraisTransport(float fraisTransport) {
		this.fraisTransport = fraisTransport;
	}
	public float getFraisJournee() {
		return fraisJournee;
	}
	public void setFraisJournee(float fraisJournee) {
		this.fraisJournee = fraisJournee;
	}
	public float getFraisRestauration() {
		return fraisRestauration;
	}
	public void setFraisRestauration(float fraisRestauration) {
		this.fraisRestauration = fraisRestauration;
	}
	
	
	
}
