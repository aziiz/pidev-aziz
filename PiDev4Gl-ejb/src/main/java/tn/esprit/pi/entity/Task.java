package tn.esprit.pi.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="T_Task")
public class Task implements Serializable{
	

	private static final long serialVersionUID = -357738161698377833L;

	
	
	public Set<WorkDay> getWorkday() {
		return workday;
	}

	public void setWorkday(Set<WorkDay> workday) {
		this.workday = workday;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="T_ID")
	int id; 
	
	@Column(name="T_NAME")
	String name;

	@Column(name="T_ESTIMATEDCOST")
	int estimatedCost;
	
	@Column(name="T_REALCOST")
	int realCost;
	
	@Column(name="T_STATE")
	String state;
	
	
	@ManyToOne
	@JoinColumn(name="FK_P_ID")
	Project projet; 

	@OneToMany(mappedBy="task" , cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<WorkDay> workday;
	
	public Project getProjet() {
		return projet;
	}

	public void setProjet(Project projet) {
		this.projet = projet;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(int estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public int getRealCost() {
		return realCost;
	}

	public void setRealCost(int realCost) {
		this.realCost = realCost;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Task() {
		super();
	}
	
	
	
	
	
	
}
