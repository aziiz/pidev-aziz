package tn.esprit.pi.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="T_TIMESHEET")
public class TimeSheet implements Serializable{


	private static final long serialVersionUID = -357738161698377833L;

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="TI_ID")
	int id;

	
	@ManyToOne
	@JoinColumn(name="FK_P_ID")
	Project projet; 
	
	
	@ManyToMany(mappedBy="timesheets", cascade = CascadeType.ALL)
	private Set<WorkDay> workday;
	
	public Project getProjet() {
		return projet;
	}

	public void setProjet(Project projet) {
		this.projet = projet;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TimeSheet() {
		super();
	}
	public Set<WorkDay> getWorkday() {
		return workday;
	}

	public void setWorkday(Set<WorkDay> workday) {
		this.workday = workday;
	}
	
}
