package tn.esprit.pi.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="FACTURE")
public class Facture implements Serializable{
	

	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	private static final long serialVersionUID = -357738161698377833L;

	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="F_ID")
	int id;
	
	@Column(name="M_MONTANT")
	float montant;
	
	@ManyToOne
	@JoinColumn(name="FK_M_ID")
	Mission mission;

	@Column(name="F_MATRICULEFISCALE")
	int matriculeFiscale;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public int getMatriculeFiscale() {
		return matriculeFiscale;
	}

	public void setMatriculeFiscale(int matriculeFiscale) {
		this.matriculeFiscale = matriculeFiscale;
	}

	public Facture() {
		super();
	}
	
	public float getMontant() {
		return montant;
	}

	public void setMontant(float montant) {
		this.montant = montant;
	}
	

}
